Source: python-anndata
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Diane Trout <diane@ghic.org>,
           Steffen Moeller <moeller@debian.org>
Section: python
Priority: optional
Build-Depends: dh-sequence-python3,
               python3-setuptools,
               python3-all,
               debhelper-compat (= 13),
               pybuild-plugin-pyproject,
               python3-distributed <!nocheck>,
               python3-h5py,
               python3-importlib-metadata,
               python3-hatchling,
               python3-hatch-vcs,
               python3-natsort,
               python3-numpy,
               python3-pandas,
               python3-packaging,
               python3-scipy,
               python3-setuptools-scm,
               python3-pytest <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-pytest-mock <!nocheck>,
               python3-array-api-compat <!nocheck>,
               python3-boltons <!nocheck>,
               python3-importlib-metadata <!nocheck>,
               python3-joblib <!nocheck>,
               python3-httpx <!nocheck>,
               python3-openpyxl <!nocheck>,
               python3-sklearn <!nocheck>,
               python3-xlrd <!nocheck>,
               python3-zarr <!nocheck>,
               flit
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/python-anndata
Vcs-Git: https://salsa.debian.org/med-team/python-anndata.git
Homepage: https://github.com/theislab/anndata
Rules-Requires-Root: no

Package: python3-anndata
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python3-openpyxl
Description: annotated gene by sample numpy matrix
 AnnData provides a scalable way of keeping track of data together
 with learned annotations. It is used within Scanpy, for which it was
 initially developed. Both packages have been introduced in Genome
 Biology (2018).
